
OVERVIEW
--------

The Multiforms module was designed to handle contests and surveys for the
website of The Georgia Straight, a weekly Vancouver newspaper.

  Features:
    * Multiple form support
    * Ability to save/load form "snippets" (groups of fields)
    * Duplicate submission prevention
      (based on specification of which fields should collectively be unique)
    * Ability to purge submitted data
    * Supports closing date and message
    * Integrated email referrals
    * Supports completion threshold
    * Creation of draw entries upon successful complete
    * Creation of extra draw entries for each email referral 


INSTALLING MULTIFORMS
---------------------

1. Visit http://drupal.org/project/multiforms and download and decompress the
   appropriate archive.

2. Place the resulting "multiforms" folder in your module directory.

3. Enable the three component modules via the admin/build/modules
   administration page.


DOCUMENTATION
-------------

Documentation is available at the Multiforms handbook page:
http://drupal.org/node/145576
